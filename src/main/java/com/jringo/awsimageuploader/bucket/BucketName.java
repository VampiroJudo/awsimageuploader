package com.jringo.awsimageuploader.bucket;

public enum BucketName {

    PROFILE_IMAGE("jringo-image-upload");

    private final String bucketName;

    BucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }
}
