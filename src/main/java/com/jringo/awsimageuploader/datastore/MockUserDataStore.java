package com.jringo.awsimageuploader.datastore;

import com.jringo.awsimageuploader.profile.UserProfile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Repository
public class MockUserDataStore {

    private static final List<UserProfile> USER_PROFILES = new ArrayList<>();

    static {
        USER_PROFILES.add(new UserProfile(UUID.fromString("4de405e6-b000-42b4-a08e-25cd7f30f538"), "John Conley", null));
        USER_PROFILES.add(new UserProfile(UUID.fromString("e09f935d-6017-4571-91dc-94ccde417faa"), "Yesenia Ceballos", null));
        USER_PROFILES.add(new UserProfile(UUID.fromString("48d01d17-32cc-4a8b-9c1e-216cb6bd89e2"), "Gus Grissom", null));
    }

    public List<UserProfile> getUserProfiles() {
        return USER_PROFILES;
    }
}
