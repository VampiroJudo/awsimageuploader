package com.jringo.awsimageuploader.profile;


import com.jringo.awsimageuploader.bucket.BucketName;
import com.jringo.awsimageuploader.filestore.FileStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

import static org.apache.http.entity.ContentType.*;

@Service
public class UserProfileService {

    private final DataAccessService dataAccessService;
    private final FileStore fileStore;

    @Autowired
    public UserProfileService(DataAccessService dataAccessService, FileStore fileStore) {
        this.dataAccessService = dataAccessService;
        this.fileStore = fileStore;
    }

    List<UserProfile> getUserProfiles() {
        return dataAccessService.getUserProfiles();
    }

    public void uploadUserImage(UUID userProfileId, MultipartFile file) {
        // 1. Check if image is not empty
        isFileEmpty(file);
        // 2. If file is an image
        isImage(file);

        // 3. The user exists in our database
        UserProfile user = getUserProfileOrThrow(userProfileId);

        // 4. Grab some metadata from file if any
        Map<String, String> metadata = extractMetadata(file);

        // 5. Store the image in s3 and update database (userImageLink) with s3 image link
        String path = String.format("%s/%s", BucketName.PROFILE_IMAGE.getBucketName(), user.getUserProfileId());
        String filename = String.format("%s-%s", file.getOriginalFilename(), UUID.randomUUID());

        try {
            fileStore.save(path, filename, Optional.of(metadata), file.getInputStream());
            user.setUserImageLink(filename);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

        //Check to see if file is an image
        private void isImage(MultipartFile file) {
            if (!Arrays.asList(
                    IMAGE_JPEG.getMimeType(),
                    IMAGE_PNG.getMimeType(),
                    IMAGE_GIF.getMimeType()).contains(file.getContentType())) {
                throw new IllegalStateException("File must be an image [" + file.getContentType() + "]");
            }
        }

        private UserProfile getUserProfileOrThrow(UUID userProfileId) {
            return dataAccessService
                    .getUserProfiles()
                    .stream()
                    .filter(userProfile -> userProfile.getUserProfileId().equals(userProfileId))
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException(String.format("User profile %s not found", userProfileId)));
        }

        // Grab metadata
        private Map<String, String> extractMetadata(MultipartFile file) {
            Map<String, String> metadata = new HashMap<>();
            metadata.put("Content-Type", file.getContentType());
            metadata.put("Content-Length", String.valueOf(file.getSize()));
            return metadata;
        }

        // Store image in s3 bucket and update DB
        private void isFileEmpty(MultipartFile file) {
            if (file.isEmpty()) {
                throw new IllegalStateException("Cannot upload empty file [ " + file.getSize() + "]");
            }
        }

     byte[] downloadUserImage(UUID userProfileId) {
        UserProfile user = getUserProfileOrThrow(userProfileId);
        String path = String.format(
                "%s/%s",
                BucketName.PROFILE_IMAGE.getBucketName(),
                user.getUserProfileId());

         return   user.getUserImageLink()
                    .map(key -> fileStore.download(path, key))
                    .orElse(new byte[0]);
    }
}
