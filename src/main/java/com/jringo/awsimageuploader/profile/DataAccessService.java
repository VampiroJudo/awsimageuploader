package com.jringo.awsimageuploader.profile;

import com.jringo.awsimageuploader.datastore.MockUserDataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DataAccessService {

    private final MockUserDataStore mockUserDataStore;

    @Autowired
    public DataAccessService(MockUserDataStore mockUserDataStore) {
        this.mockUserDataStore = mockUserDataStore;
    }

    List<UserProfile> getUserProfiles() {
        return mockUserDataStore.getUserProfiles();
    }
}
